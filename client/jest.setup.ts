import { server } from "./src/mocks/server";

beforeAll(() => server.listen());
afterAll(() => server.close());

// they don't affect other tests.
afterEach(() => server.resetHandlers());
