import { render, screen, waitFor } from "@testing-library/react";
// import userEvent from "@testing-library/user-event";
import Index from "..";
import App from "../_app";
test("loads and displays greeting", async () => {
  render(
    <App>
      <Index />
    </App>
  );

  expect(screen.getByRole("main")).toHaveTextContent("Loading...");
  await waitFor(() => screen.getByText("John"));
  expect(screen.getByRole("main")).toHaveTextContent("Hi");
});
