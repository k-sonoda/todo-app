import { PrismaClient } from "@prisma/client";
const prisma = new PrismaClient();

async function main() {
  // Delete all `User` and `Message` records
  await Promise.all([
    prisma.message.deleteMany({}),
    prisma.user.deleteMany({}),
  ]);
  // (Re-)Create dummy `User` and `Message` records
  await Promise.all([
    prisma.user.create({
      data: {
        name: "Jack",
        messages: {
          create: [
            {
              body: "A Note for Jack",
            },
            {
              body: "Another note for Jack",
            },
          ],
        },
      },
    }),
    prisma.user.create({
      data: {
        name: "Ryan",
        messages: {
          create: [
            {
              body: "A Note for Ryan",
            },
            {
              body: "Another note for Ryan",
            },
          ],
        },
      },
    }),
    prisma.user.create({
      data: {
        name: "Adam",
        messages: {
          create: [
            {
              body: "A Note for Adam",
            },
            {
              body: "Another note for Adam",
            },
          ],
        },
      },
    }),
  ]);
}

main().then(() => {
  console.log("Data seeded...");
});
