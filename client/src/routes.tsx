import React, { Fragment, lazy, Suspense } from "react";
import { Routes as BrowserRoutes, Route } from "react-router-dom";

type Component = React.ExoticComponent<{
  children?: React.ReactNode | undefined;
}>;
const PRESERVED = import.meta.glob<true, string, { default: Component }>(
  "/src/pages/(_app|404).tsx",
  { eager: true }
);
const ROUTES = import.meta.glob<false, string, { default: Component }>(
  "/src/pages/**/[a-z[]*.tsx"
);

const preserved = Object.keys(PRESERVED).reduce<Record<string, Component>>(
  (preserved, file) => {
    const key = file.replace(/\/src\/pages\/|\.tsx$/g, "");
    return { ...preserved, [key]: PRESERVED[file].default };
  },
  {}
);

export const routes = Object.keys(ROUTES).map((route) => {
  const path = route
    .replace(/\/src\/pages|index|\.tsx$/g, "")
    .replace(/\[\.{3}.+\]/, "*")
    .replace(/\[(.+)\]/, ":$1");

  return { path, component: lazy(ROUTES[route]), preload: ROUTES[route] };
});

export const Routes = () => {
  const App = preserved?.["_app"] || Fragment;
  const NotFound = preserved?.["404"] || Fragment;

  return (
    <App>
      <Suspense fallback={"Loading..."}>
        <BrowserRoutes>
          {routes.map(({ path, component: Component = Fragment }) => (
            <Route key={path} path={path} element={<Component />} />
          ))}
          <Route path="*" element={<NotFound />} />
        </BrowserRoutes>
      </Suspense>
    </App>
  );
};
