import "../index.css";
import React from "react";
import { QueryClient, QueryClientProvider } from "@tanstack/react-query";
import { GraphQLClient } from "graphql-request";

export default function App({ children }: { children: React.ReactNode }) {
  const queryClient = new QueryClient();
  return (
    <QueryClientProvider client={queryClient}>
      <header></header>
      <main>{children}</main>
      <footer></footer>
    </QueryClientProvider>
  );
}

export const graphQLClient = new GraphQLClient(
  process.env.API_URL || "http://localhost:4000/graphql"
);
