import { useCallback, useEffect, useMemo, useRef, useState } from "react";

import { Link as RouterLink, LinkProps, To } from "react-router-dom";

import { routes } from "../routes";

const getMatchingRoute = (path: To) => {
  const routeDynamicSegments = /:\w+|\*/g;
  if (typeof path === "object") throw new Error(`${path} not supported`);
  return routes.find(
    (route) =>
      path.match(
        new RegExp(route.path.replace(routeDynamicSegments, ".*"))
      )?.[0] === path
  );
};

export const Link = ({
  children,
  to,
  prefetch = true,
  ...props
}: LinkProps & { prefetch: boolean }) => {
  const ref = useRef<HTMLAnchorElement>(null);
  const [prefetched, setPrefetched] = useState(false);

  const route = useMemo(() => getMatchingRoute(to), [to]);
  const preload = useCallback(
    () => route?.preload() && setPrefetched(true),
    [route]
  );
  const prefetchable = Boolean(route && !prefetched);

  useEffect(() => {
    if (prefetchable && prefetch && ref?.current) {
      const observer = new IntersectionObserver(
        (entries) =>
          entries.forEach((entry) => entry.isIntersecting && preload()),
        { rootMargin: "200px" }
      );

      observer.observe(ref.current);
      return () => observer.disconnect();
    }
  }, [prefetch, prefetchable, preload]);
  const handleMouseEnter = () => prefetchable && preload();
  return (
    <RouterLink ref={ref} to={to} onMouseEnter={handleMouseEnter} {...props}>
      {children}
    </RouterLink>
  );
};
