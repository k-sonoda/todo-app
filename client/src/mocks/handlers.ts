import { mockGetUsersQuery } from "../graphql/generated";

export const handlers = [
  mockGetUsersQuery((_req, res, ctx) => {
    return res(
      ctx.data({
        users: [{ name: "John", messages: [{ body: "Hi" }] }],
      })
    );
  }),
];
