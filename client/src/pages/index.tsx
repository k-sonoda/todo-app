import { User, Message } from "../types";
import { GetUsersDocument, GetUsersQuery } from "../graphql/generated";
import { useQuery } from "@tanstack/react-query";
import { graphQLClient } from "./_app";

export default function Index() {
  const { status, data, error } = useQuery<GetUsersQuery, Error>(
    ["users"],
    () => graphQLClient.request(GetUsersDocument)
  );

  switch (status) {
    case "loading":
      return <span>Loading...</span>;
    case "error":
      return <span>Error: {error.message}</span>;
    default:
      return (
        <div className="flex flex-col w-full border-opacity-50">
          {data.users.map((user, i) => (
            <UserDisplay user={user} key={i} />
          ))}
        </div>
      );
  }
}

function MessageDisplay({ message }: { message: Message; index: number }) {
  return <li>{message.body}</li>;
}

function UserDisplay({ user }: { user: User }) {
  return (
    <>
      <p className="divider">{user.name}</p>
      <ul className="grid h-20 card bg-base-300 rounded-box place-items-center">
        {user.messages.map((message, i) => (
          <MessageDisplay key={i} index={i} message={message} />
        ))}
      </ul>
    </>
  );
}
