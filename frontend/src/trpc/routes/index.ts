import { publicProcedure, router } from '../../trpc';
import { observable } from '@trpc/server/observable';
import { z } from 'zod';

export const appRouter = router({
  greet: publicProcedure.input(z.string()).query(({ input }) => ({ greeting: `hello, ${input}!` })),
  greetWs: publicProcedure.input(z.string()).subscription(({ input }) => {
    return observable<{ greeting: string }>((emit) => {
      const interval = setInterval(() => {
        emit.next({ greeting: `hello, ${input}!` });
      }, 1000);

      return () => {
        clearInterval(interval);
      };
    });
  }),
});

export type AppRouter = typeof appRouter;
