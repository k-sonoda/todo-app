import { StrictMode } from "react";
import { createRoot } from "react-dom/client";
import { BrowserRouter } from "react-router-dom";
import { Routes } from "./routes";

const root = document.getElementById("root");
if (!root) throw new Error("#root not found");

import { worker } from "./mocks/browser";
if (process.env.NODE_ENV === "test") worker.start();

createRoot(root).render(
  <StrictMode>
    <BrowserRouter>
      <Routes />
    </BrowserRouter>
  </StrictMode>
);
